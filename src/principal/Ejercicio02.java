package principal;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Ejercicio02 {
    /*2. Tener una array de 10 numeros enteros (Positivos y Negativos), Recorrer cada numero del array y
mostrar su cuadrado, repetir el proceso mientras el numero no sea negativo.*/

    public static void main(String[] args) {

        //Array con numeros
        List<Integer> numeros = Arrays.asList(5,6,9,4,-1,3,5,-6);

        //Evaluamos si el numero es positivo
        Predicate<Integer> condicion = (x) -> x>0;

        //Funcion para elevar el numero al cuadrado y devolver una cadena
        Function<Integer,String> cuadrado = (var) -> "El cuadrado de "+var+" es: "+(var*var);

        //Consumer para enviar el array numeros e iterar la condicion y si cumple efectuar el cuadrado
        Consumer<List<Integer>> efectuar = List -> {
            for (Integer num: List) {

                if(condicion.test(num)){
                    //mostrando mensaje de la funcion cuadrado
                    System.out.println(cuadrado.apply(num));
                }else{
                    break;
                }
            }
        };

        //ejecutando el Consumer, enviando el array de numeros como parametro
        efectuar.accept(numeros);
    }

}
