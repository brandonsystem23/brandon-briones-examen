package principal;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class Ejercicio01 {
    /*1. Crear un array de notas entre 0 a 20 y hacer una logica para recorrer el array e imprimir en pantalla:
Insuficiente cuando nota esta entre 0 y 10,
Suficiente 11 a 13, Bien 14 a 17 y
Excelente 18 a 20*/

    public static void main(String[] args) {

        //Lista de notas
        List<Double> notas = Arrays.asList(15.50d, 8.50d, 19.00d, 11.50d );

        //Funcion para evaluar la nota del alumno
        Function<Double , String> evaluarNota = (nota) -> {
            if(nota >= 0 && nota<=10){
                return "Nota: "+nota+" es Insuficiente";
            }
            else if(nota >= 11 && nota<=13){
                return "Nota: "+nota+" es Suficiente";
            }
            else if(nota >= 14 && nota<=17){
                return "Nota: "+nota+" es Bien";
            }
            else if(nota >= 18 && nota<=20){
                return "Nota: "+nota+" es Excelente";
            }
            else{
                return "Nota fuera del rango";
            }
        };

        //Consumer para enviar el array de notas e iterar la evaluacion
        Consumer<List<Double>> efectuar = List -> {
            for (Double num: List) {
                System.out.println(evaluarNota.apply(num));
            }
        };

        //ejecutando el Consumer, enviando el array de notas como parametro
        efectuar.accept(notas);




    }




}
