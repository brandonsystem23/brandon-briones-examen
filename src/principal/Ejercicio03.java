package principal;

public class Ejercicio03 {
    /* 3.  Crea una clase llamada CuentaBancaria que tendrá los siguientes atributos:
    titular y cantidad (puede tener decimales).
    El titular será obligatorio y la cantidad es opcional. Crea dos constructores que cumpla lo anterior.
    Crea sus métodos get, set y toString.
    Tendrá dos métodos especiales:
    ingresar(double cantidad): se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa,
    no se hará nada.
    retirar(double cantidad): se retira una cantidad a la cuenta, si restando la cantidad actual a la que
    nos pasan es negativa, la cantidad de la cuenta pasa a ser 0.
    * Cree un objecto de esta clase y pruebe los metodos anteriores.*/
    public static void main(String[] args) {

        //Creando el objeto CuentaBancaria con parametro cantidad opcional
        //Titular: Brandon Briones Rojas
        //Cantidad: S/.500.00
        CuentaBancaria cuenta = new CuentaBancaria("Brandon Briones Rojas",500.00);
        //Mostrando cuenta actual
        System.out.println("Cuenta actual\n"+cuenta.toString()+"\n");

        //1. Se realizara un Ingreso de dinero negativo
        cuenta.Ingresar(-9.50);
        //Mostrando cuenta actual (valor negativo no se suma a la cantidad)
        System.out.println("Cuenta ingresando monto negativo\n"+cuenta.toString()+"\n");

        //2. Se realizara un Ingreso de dinero positiva
        cuenta.Ingresar(10.50);
        //Mostrando cuenta actual (valor positivo si se suma a la cantidad)
        System.out.println("Cuenta ingresando monto positivo\n"+cuenta.toString()+"\n");

        //3. Se realizara un Retiro de dinero menor a la cantidad
        cuenta.Retirar(15.50);
        //Mostrando cuenta actual (valor menor a la cantidad, entonces se le resta a la cantidad)
        System.out.println("Cuenta retirando monto menor a cantidad\n"+cuenta.toString()+"\n");

        //4. Se realizara un Retiro de dinero mayor a la cantidad
        cuenta.Retirar(800.50);
        //Mostrando cuenta actual (valor mayor a la cantidad, entonces cantidad se iguala a cero)
        System.out.println("Cuenta retirando monto mayor a cantidad\n"+cuenta.toString()+"\n");


    }



}
