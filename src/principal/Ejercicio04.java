package principal;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Ejercicio04 {

    public static void main(String[] args) {
    /*4. Crear un array de 10 números enteros (9,5,-7,12,33,1,45,94,0,2) y
    mostrar el numero mayor y el numero menor.
     */

        //Array de numeros
        List<Integer> numeros = Arrays.asList(9,5,-7,12,33,1,45,94,0,2);

        //Mostrar el numero menor (ordenar ascendente y mostrar el primero)
        numeros.stream().sorted().limit(1).forEach(Ejercicio04::MostrarMenor);

        //Mostrar el numero mayor (ordenar descendente y mostrar el primero)
        numeros.stream().sorted(Comparator.reverseOrder()).limit(1).forEach(Ejercicio04::MostrarMayor);
    }

    //FUNCION ESTATICA PARA MOSTRAR NUMERO MENOR
    public static void MostrarMenor(Integer num){
        System.out.println("El numero menor es: "+num);
    }

    //FUNCION ESTATICA PARA MOSTRAR NUMERO MAYOR
    public static void MostrarMayor(Integer num){
        System.out.println("El numero mayor es: "+num);
    }
}
