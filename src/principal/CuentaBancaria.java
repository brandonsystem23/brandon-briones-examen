package principal;

public class CuentaBancaria {
    /* 3.  Crea una clase llamada CuentaBancaria que tendrá los siguientes atributos:
    titular y cantidad (puede tener decimales).
    El titular será obligatorio y la cantidad es opcional. Crea dos constructores que cumpla lo anterior.
    Crea sus métodos get, set y toString.
    Tendrá dos métodos especiales:
    ingresar(double cantidad): se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa,
    no se hará nada.
    retirar(double cantidad): se retira una cantidad a la cuenta, si restando la cantidad actual a la que
    nos pasan es negativa, la cantidad de la cuenta pasa a ser 0.
    * Cree un objecto de esta clase y pruebe los metodos anteriores.*/

    //Atributos
    private String titular;
    private Double cantidad;

    //Constructores con el parametro cantidad opcional y titular obligatorio
    public CuentaBancaria(String titular){
        this.titular = titular;
        this.cantidad = 0.00;
    }

    public CuentaBancaria(String titular, Double cantidad) {
        this.titular = titular;
        this.cantidad = cantidad;
    }

    //******FUNCIONES ESPECIALES ***************
    /* 1. ingresar(double cantidad): se ingresa una cantidad a la cuenta, si la cantidad introducida es negativa,
    no se hará nada.*/

    public void Ingresar(Double cantidad){
        CalculosCuenta calculo = (num) -> {
            this.cantidad += (num > 0 ? num : 0);
        };

        calculo.Operacion(cantidad);
    }

    /* 2. retirar(double cantidad): se retira una cantidad a la cuenta, si restando la cantidad actual a la que
    nos pasan es negativa, la cantidad de la cuenta pasa a ser 0. */

    public void Retirar(Double cantidad){
        CalculosCuenta calculo = (num) -> {
            this.cantidad -= (num > this.cantidad ? this.cantidad : num);
        };

        calculo.Operacion(cantidad);
    }


    //Getters and Setters
    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    //Funcion ToString
    @Override
    public String toString() {
        return "CuentaBancaria{" +
                "titular='" + titular + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}
